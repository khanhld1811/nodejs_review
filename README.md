## Enviroment
 - NodeJS version: 12.16.3
 - SQL Server 2019 version: 15.0
 
## Structure

```
    .
    ├── configs             # Environment variables and configuration related stuff
    ├── controllers         # Takes user request and tells the services what to do
    ├── middleware          # Request middlewares
    ├── repository          # Organize database query statements
    ├── responses           # 
    ├── routes              # Were done using express to connect urls (user's get and post requests) to controllers
    ├── services            # All the business logic is here
    ├── utils               # Util libs (formats, validation, etc)
    ├── index.js            # App entry point
    ├── env.json            # 
    ├── package-lock.json   #
    ├── package.json        #
    └── README.md           #